import sys

class Token():
    def __init__(self):
        self.tokens = [
            (r'[$]([a-z])*[^\d]',"VARIABLE"),
            (r'[:][:][:][^\d]([A-Za-z])*[^\d][:][:][:]',"COMENTARIO"),
            (r'[-][>]*(escriba)','FUNCION'),
            (r'[-][>]*(igual)', 'ASIGNACION'),
            (r'[-][>]*(igual)*[+]', 'COMPARACION'),
            (r'[-][>]*(suma)', 'SUMA'),
            (r'[-][>]*(resta)', 'RESTA'),
            (r'[-][>]*(para)', 'CICLO'),
            (r'[-][>]*(mientras)', 'CICLO'),
            (r'[-][>]*(si)', 'CONDICION'),
            (r'[-][>]*(no)', 'CONDICION'),
            (r'[-][>]*(mult)', 'MULTIPLICACION'),
            (r'[-][>]*(div)', 'DIVICION'),
            (r'::', 'DELIMITADOR'),
            (r':', 'DELIMITADOR'),
            (r'\(', 'PARENTESIS'),
            (r'\)', 'PARENTESIS'),
            (r'\d{1,}', 'ENTERO'),
            (r'(\")(.*)(\")', 'CADENA'),
            (r'[ \n\t]+', None)
        ]
    def getToken(pos):
        return self.tokens[pos]