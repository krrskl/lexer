import sys

string = open(sys.argv[1]).read()

tokens = [
    (r'[$]([a-z])*[^\d]',"VARIABLE"),
	(r'[:][:][:][^\d]([A-Za-z])*[^\d][:][:][:]',"COMENTARIO"),
	(r'[-][>]*([e][s][c][r][i][b][a])','FUNCION'),
	(r'[-][>]*([i][g][u][a][l])', 'ASIGNACION'),
	(r'[-][>]([i][g][u][a][l])[+]', 'COMPARACION'),
	(r'[-][>]*([s][u][m][a])', 'SUMA'),
	(r'[-][>]*([r][e][s][t][a])', 'RESTA'),
	(r'[-][>]*([p][a][r][a])', 'CICLO'),
	(r'[-][>]*([m][i][e][n][t][r][a][s])', 'CICLO'),
	(r'[-][>]*([s][i])', 'CONDICION'),
	(r'[-][>]*([n][o])', 'CONDICION'),
	(r'[-][>]*([m][u][l][t])', 'MULTIPLICACION'),
	(r'[-][>]*([d][i][v])', 'DIVICION'),
	(r'::', 'DELIMITADOR'),
	(r':', 'DELIMITADOR'),
	(r'\(', 'PARENTESIS'),
	(r'\)', 'PARENTESIS'),
	(r'\d{1,}', 'ENTERO'),
	(r'(\")(.*)(\")', 'CADENA'),
	(r'[ \n\t]+', None),
]

for x in range(len(string)):
	print(string[x])
