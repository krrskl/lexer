import os

class voz():
  def __init__(self,texto, error):
    self.texto=texto
    self.error=error

  def hablarValido(self):
    os.system('espeak "Palabra válida '+str(self.texto)+", tipo "+str(self.error)+'" -s 100 -v es-la')

  def hablarError(self):
    os.system('espeak "Error en la línea '+str(self.error)+", palabra "+str(self.texto)+'" -s 100 -v es-la')

